declare type Graph = Map<string, Map<string, boolean>>;
export default class ChannelGraph {
  private myXpub: string;
  private graph: Graph = new Map();

  constructor(myXpub: string) {
    this.myXpub = myXpub;
    this.setConnection(myXpub);
  }

  importGraph(graph: { [addr: string]: string[] }) {
    // merge instead of replace all
    Object.entries(graph).forEach(e => {
      let node: Map<string, boolean>;
      if (this.graph.has(e[0])) {
        node = this.graph.get(e[0])!;
      } else {
        node = new Map();
      }
      e[1].forEach(p => node.set(p, true));
      this.graph.set(e[0], node);
    });
  }

  exportGraph() {
    const ret: { [addr: string]: string[] } = {};
    [...this.graph].sort().forEach(v => {
      ret[v[0]] = Array.from(v[1].keys()).sort();
    });
    return ret;
  }

  setConnection(xpub1: string, xpub2?: string) {
    if (this.graph.has(xpub1) && xpub2) {
      const node = this.graph.get(xpub1)!;
      node.set(xpub2, true);
      this.graph.set(xpub1, node); // not sure if needed
    } else if (!this.graph.has(xpub1)) {
      this.graph.set(xpub1, xpub2 ? new Map([[xpub2, true]]) : new Map());
    }

    if (xpub2) {
      if (this.graph.has(xpub2)) {
        const node = this.graph.get(xpub2)!;
        node.set(xpub1, true);
        this.graph.set(xpub2, node); // not sure if needed
      } else {
        this.graph.set(xpub2, new Map([[xpub1, true]]));
      }
    }
  }

  printGraph() {
    console.log(`I'm ${this.myXpub.slice(6, 12)}`);
    let result = "";
    this.graph.forEach((v, k) => {
      const str = Array.from(v.keys())
        .map(a => a.slice(6, 12))
        .join(", ");
      result += `[${k.slice(6, 12)}] ${str}\n`;
    });
    console.log(result);
  }

  getRandomCfAddr(): string {
    const myNeibor = this.getMyConnections();
    const peerList = Array.from(this.graph.keys()).filter(addr => {
      const notMyNeibor = !myNeibor.includes(addr);
      const notMyself = addr !== this.myXpub;
      return notMyNeibor && notMyself;
    }); // without myself and already connected
    if (peerList.length === 0) {
      return "";
    }
    const index = Math.floor(Math.random() * peerList.length);
    return peerList[index];
    // return peerList[0];
  }

  getConnections(xpub: string): string[] {
    if (this.graph.has(xpub)) {
      return Array.from(this.graph.get(xpub)!.keys());
    }
    return [];
  }

  getMyConnections(): string[] {
    return this.getConnections(this.myXpub);
  }

  private sortGraph() {
    // make sure always find SAME shortest path
    for (const [k, v] of this.graph.entries()) {
      const sorted = new Map([...v.keys()].sort().map(e => [e, true]));
      this.graph.set(k, sorted);
    }
    this.graph = new Map(
      [...this.graph.entries()].sort((a, b) =>
        [a[0], b[0]].sort().join() === [a[0], b[0]].join() ? -1 : 1
      )
    );
  }

  breadFirstSearch(from: string, to: string): string[] {
    // safety!
    if (!this.graph.has(from)) {
      return [];
    }

    this.sortGraph();
    const predNode = new Map<string, string>([[from, ""]]);
    const queue = [from];
    while (!predNode.has(to) && queue.length > 0) {
      const start = queue.shift()!;
      const neibors = Array.from(this.graph.get(start)!.keys());
      neibors.forEach(n => {
        if (!predNode.has(n)) {
          queue.push(n);
          predNode.set(n, start);
        }
      });
    }
    if (!predNode.has(to)) {
      return [];
    }
    const path: string[] = [to];
    while (!path.includes(from)) {
      const pre = predNode.get(path[0])!;
      path.unshift(pre);
    }
    return path;
  }

  getIntermediariesBetweenMeAnd(responding: string): string[] {
    // let isVisited = new Map<string, Boolean>();
    // let pathList: string[] = [this.myXpub];
    // this.findPath(this.myXpub, responding, isVisited, pathList);
    // console.log(pathList);
    // if(!this.graph.has(responding)) {
    //   return [];
    // }
    // let ret: string[][] = [];
    // const graphCopy = new Map(this.graph);
    // let neibors = Array.from(graphCopy.get(responding)!.keys() || []);
    // while(neibors.length > 0) {
    //   const res = this.BFS(this.myXpub, responding, graphCopy);
    //   ret.push(res);
    //   const n = neibors.shift()!;
    //   console.log(n)
    //   graphCopy.get(responding)!.delete(n);
    // }
    // return ret;
    const result = this.breadFirstSearch(this.myXpub, responding);
    return result;
    // console.log(result);
    // return result.filter(x => x !== this.myXpub && x !== responding); // only find shortest now
  }
}

// test
// const cg = new ChannelGraph('0');
// cg.setConnection('0', '1');
// cg.setConnection('0', '4');
// cg.setConnection('0', '5');
// //cg.setConnection('1', '7');
// cg.setConnection('4', '3');
// cg.setConnection('5', '6');
// cg.setConnection('3', '2');
// cg.setConnection('3', '6');
// cg.setConnection('2', '7');
// cg.setConnection('6', '7');
// const whee = cg.getIntermediariesBetweenMeAnd('7');
// console.log(whee);
