import { IMessagingService, NodeMessage } from "@counterfactual/node/dist";
import { EventEmitter } from "events";
import Websocket from "ws";

import ChannelGraph from "./ChannelGraph";
import {
  CFMessage,
  CFMessageTypes,
  OfferPeerPayload,
  P2PMessage,
  PeerId,
  PeerInfo
} from "./types";

const NodeP2P = require("./libp2p-bundle");
const PeerId = require("peer-id");
const PeerInfo = require("peer-info");

export default class P2PMessageService implements IMessagingService {
  // P2P Node ID to connection callback
  // private connectedPeer: Map<string, (msg: string) => void> = new Map();
  // Store gossiped message to prevent loop
  // private gossipMessageHashStore: string[] = [];
  private node: any;
  private publicId: string = "";
  private myCallback: (msg: NodeMessage) => void = () => {};
  private channelGraph: ChannelGraph = new ChannelGraph("");
  private p2pEvent: EventEmitter = new EventEmitter();
  // private offerInterval!: NodeJS.Timeout;
  // private peerValue: Set<string> = new Set();

  constructor() {}

  async initialize(
    publicId: string,
    port: number,
    commanderWs: Websocket
  ): Promise<OfferPeerPayload> {
    this.publicId = publicId;
    this.channelGraph = new ChannelGraph(this.publicId);

    const myId: PeerId = await new Promise((res, rej) => {
      PeerId.create((err: Error, id: PeerId) => {
        if (err) rej(err);
        res(id);
      });
    });

    const peerMe = new PeerInfo(myId);
    peerMe.multiaddrs.add(`/ip4/0.0.0.0/tcp/${port}`);

    this.node = new NodeP2P({ peerInfo: peerMe });

    await new Promise((res, rej) => {
      // await node start
      this.node.start((err: Error) => {
        if (err) rej(err);
        res();
      });
    });

    const addr: string[] = [];
    peerMe.multiaddrs.forEach((e: any) => {
      addr.push(e.toString());
    });

    // offer p2p address
    commanderWs.send(
      JSON.stringify({
        id: this.publicId,
        type: CFMessageTypes.INTRODUTION,
        payload: { addr, id: myId.toB58String(), cfId: publicId }
      } as CFMessage)
    );

    const payload: OfferPeerPayload = await new Promise(r =>
      commanderWs.once("message", (data: string) => {
        const msg: CFMessage = JSON.parse(data);
        if (msg.type === CFMessageTypes.OFFER_PEERS) {
          r(msg.payload);
        }
      })
    );
    // console.error(`Got ${payload.p2pPeers.length} peers to connect`);
    for (const peer of payload.p2pPeers) {
      if (peer.id === myId.toB58String()) {
        // prevent dial myself
        continue;
      }
      const peerInfo = new PeerInfo(PeerId.createFromB58String(peer.id));
      peer.addr.forEach(a => peerInfo.multiaddrs.add(a));
      await new Promise((res, rej) => {
        this.node.dial(peerInfo, (err: Error) => {
          if (err) {
            rej(err);
          }
          res();
        });
      });
    }

    this.node.pubsub.subscribe(
      "counterfactual",
      (msg: any) => {
        const objectify = JSON.parse(msg.data.toString());
        this.p2pMessageHandler(objectify as P2PMessage);
      },
      () => {}
    );

    return payload;
  }

  async stop() {
    return await new Promise((res, rej) => {
      // await node start
      this.node.stop((err: Error) => {
        if (err) rej(err);
        res();
      });
    });
  }

  private objectToBuffer(obj: any) {
    return Buffer.from(JSON.stringify(obj));
  }

  private p2pMessageHandler(msg: P2PMessage) {
    // if (this.isMessageGossiped(msg)) return;
    switch (msg.action) {
      case "protocolMessage":
        const nodeMessage = msg.payload as NodeMessage;
        if (msg.toCFAddr.includes(this.publicId)) {
          this.myCallback(nodeMessage);
        }
        break;
      case "channelDepositComplete":
        const xpubs: string[] = msg.payload;
        // console.log(
        //   `Graph edge added! ${xpubs[0].substr(5, 10)} === ${xpubs[1].substr(
        //     5,
        //     10
        //   )}`
        // );
        this.channelGraph.setConnection(xpubs[0], xpubs[1]);
        this.p2pEvent.emit("graphEdgeAdded", [xpubs[0], xpubs[1]]);
        break;
      case "allOperationDone":
        const xpub: string = msg.payload;
        this.p2pEvent.emit("allOperationDone", xpub);
        break;
      default:
        console.log(`Received unkinown action ${msg.action}`);
        break;
    }
  }

  channelDepositComplete(xpubs: string[]) {
    const msg: P2PMessage = {
      action: "channelDepositComplete",
      toCFAddr: [this.publicId],
      payload: xpubs
    };
    this.p2pMessageHandler(msg);
    this.node.pubsub.publish(
      "counterfactual",
      this.objectToBuffer(msg),
      () => {}
    );
  }

  allOperationDone() {
    const msg: P2PMessage = {
      action: "allOperationDone",
      toCFAddr: [this.publicId],
      payload: this.publicId
    };
    // this.p2pMessageHandler(msg);
    this.node.pubsub.publish(
      "counterfactual",
      this.objectToBuffer(msg),
      () => {}
    );
  }

  printGraph() {
    this.channelGraph.printGraph();
  }

  getChannelGraph() {
    return this.channelGraph;
  }

  async send(to: string[], msg: NodeMessage): Promise<any> {
    this.node.pubsub.publish(
      "counterfactual",
      this.objectToBuffer({
        action: "protocolMessage",
        toCFAddr: to,
        payload: msg
      } as P2PMessage),
      () => {}
    );
  }

  onReceive(address: string, callback: (msg: NodeMessage) => void): any {
    this.myCallback = callback;
    // if(address === this.publicId) {
    //   this.myCallback = _callback;
    // }
  }

  on(event: string, callback: (...args: any[]) => void) {
    this.p2pEvent.on(event, callback);
  }

  off(event: string) {
    this.p2pEvent.removeAllListeners(event);
  }
}
