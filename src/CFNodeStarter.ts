import Axios from "axios";
import Websocket from "ws";

import CounterfactualNode from "./CounterfactualNode";
const argv = require("yargs").argv;

async function getReadyWebsocket(path: string) {
  while (true) {
    try {
      const commanderWs = new Websocket(path);
      await new Promise((res, rej) => {
        commanderWs.on("open", res);
        commanderWs.on("error", rej);
      });
      commanderWs.setMaxListeners(Number.MAX_SAFE_INTEGER);
      return commanderWs;
    } catch (error) {
      console.error("Commander not ready, retrying...");
      await new Promise(r => setTimeout(r, 2000));
    }
  }
}

async function main() {
  const commanderUrl = `http://${argv.commander}`;

  // we can do this all day
  while (true) {
    const commanderWs = await getReadyWebsocket(`${commanderUrl}/socket`);
    const roundConfig: any = (await Axios.get(
      `${commanderUrl}/currentRoundConfig`
    )).data;

    console.log(roundConfig);
    const node = new CounterfactualNode(
      roundConfig.nodes,
      roundConfig.provider,
      commanderUrl,
      commanderWs
    );
    try {
      await node.create();
    } catch (error) {
      console.error(error);
    }
    console.log("Node successfully stopped!");
    // clean up
    commanderWs.removeAllListeners();
    // grace time
    await new Promise(r => setTimeout(r, 2000));
  }
}

main();
