import { Node } from "@counterfactual/node/dist";
import {
  Address,
  AppABIEncodings,
  AppInstanceID,
  AssetType,
  ETHBucketAppState,
  NetworkContext,
  Node as NodeTypes,
  SolidityABIEncoderV2Struct
} from "@counterfactual/types";
import { BigNumber, computeAddress } from "ethers/utils";
import { fromExtendedKey, HDNode } from "ethers/utils/hdnode";
import { v4 as generateUUID } from "uuid";

function xkeyKthAddress(xkey: string, k: number): string {
  return computeAddress(xkeyKthHDNode(xkey, k).publicKey);
}

function xkeyKthHDNode(xkey: string, k: number): HDNode {
  return fromExtendedKey(xkey).derivePath(`${k}`);
}

export async function getMultisigCreationTransactionHash(
  node: Node,
  xpubs: string[]
): Promise<void> {
  const req: NodeTypes.MethodRequest = {
    requestId: generateUUID(),
    type: NodeTypes.MethodName.CREATE_CHANNEL,
    params: {
      initiatingXpub: xpubs[0],
      respondingXpub: xpubs[1]
    } as NodeTypes.CreateChannelParams
  };
  console.log(`Starting createChannel request with id ${req.requestId}`);
  await node.call(req);
}

export function makeDepositRequest(
  multisigAddress: string,
  amount: BigNumber
): NodeTypes.MethodRequest {
  return {
    requestId: generateUUID(),
    type: NodeTypes.MethodName.DEPOSIT,
    params: {
      multisigAddress,
      amount
    } as NodeTypes.DepositParams
  };
}

export async function getFreeBalanceState(
  node: Node,
  multisigAddress: string
): Promise<ETHBucketAppState> {
  const req = {
    requestId: generateUUID(),
    type: NodeTypes.MethodName.GET_FREE_BALANCE_STATE,
    params: {
      multisigAddress
    }
  };
  const response = await node.callStatic(req);
  const result = response.result as NodeTypes.GetFreeBalanceStateResult;
  return result.state;
}

export function makETHBucketVirtualAppInstanceProposalReq(
  proposedToIdentifier: string,
  initialState: SolidityABIEncoderV2Struct,
  intermediaries: string[],
  networkContext: NetworkContext
): NodeTypes.MethodRequest {
  return {
    params: {
      intermediaries,
      proposedToIdentifier,
      initialState,
      appId: networkContext.ETHBucket,
      abiEncodings: {
        stateEncoding:
          "tuple(address alice, address bob, uint256 aliceBalance, uint256 bobBalance)",
        actionEncoding: undefined
      } as AppABIEncodings,
      asset: {
        assetType: AssetType.ETH
      },
      myDeposit: new BigNumber(initialState.aliceBalance as string),
      peerDeposit: new BigNumber(initialState.bobBalance as string),
      timeout: new BigNumber(10)
    } as NodeTypes.ProposeInstallVirtualParams,
    requestId: generateUUID(),
    type: NodeTypes.MethodName.PROPOSE_INSTALL_VIRTUAL
  } as NodeTypes.MethodRequest;
}

export function makeInstallVirtualRequest(
  appInstanceId: string,
  intermediaries: Address[]
): NodeTypes.MethodRequest {
  return {
    params: {
      intermediaries,
      proposedId: appInstanceId
    } as NodeTypes.InstallVirtualParams,
    requestId: generateUUID(),
    type: NodeTypes.MethodName.INSTALL_VIRTUAL
  };
}

export function makeInstallRequest(
  appInstanceId: string
): NodeTypes.MethodRequest {
  return {
    params: {
      proposedId: appInstanceId
    } as NodeTypes.InstallParams,
    requestId: generateUUID(),
    type: NodeTypes.MethodName.INSTALL
  };
}

export function updateAppRequest(
  appInstanceId: string,
  newState: SolidityABIEncoderV2Struct
): NodeTypes.MethodRequest {
  return {
    params: {
      appInstanceId,
      newState
    } as NodeTypes.UpdateStateParams,
    requestId: generateUUID(),
    type: NodeTypes.MethodName.UPDATE_STATE
  };
}

export function makeUninstallRequest(
  appInstanceId: AppInstanceID
): NodeTypes.MethodRequest {
  return {
    params: {
      appInstanceId
    } as NodeTypes.UninstallParams,
    requestId: generateUUID(),
    type: NodeTypes.MethodName.UNINSTALL
  };
}

export function makeUninstallVirtualRequest(
  appInstanceId: AppInstanceID
): NodeTypes.MethodRequest {
  return {
    params: {
      appInstanceId
    } as NodeTypes.UninstallVirtualParams,
    requestId: generateUUID(),
    type: NodeTypes.MethodName.UNINSTALL_VIRTUAL
  };
}

export function makeTTTVirtualAppInstanceProposalReq(
  proposedByIdentifier: string,
  proposedToIdentifier: string,
  appId: Address,
  intermediaries: string[]
): NodeTypes.MethodRequest {
  return {
    params: {
      proposedToIdentifier,
      proposedByIdentifier,
      appId,
      intermediaries,
      initialState: {
        players: [
          xkeyKthAddress(proposedByIdentifier, 0),
          xkeyKthAddress(proposedToIdentifier, 0)
        ],
        turnNum: 0,
        winner: 0, // Hard-coded winner for test
        board: [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
      },
      abiEncodings: {
        stateEncoding:
          "tuple(address[2] players, uint256 turnNum, uint256 winner, uint256[3][3] board)",
        actionEncoding:
          "tuple(uint8 actionType, uint256 playX, uint256 playY, tuple(uint8 winClaimType, uint256 idx) winClaim)"
      },
      asset: {
        assetType: AssetType.ETH
      },
      myDeposit: new BigNumber(Math.floor(Math.random() * 50000 + 10000)),
      peerDeposit: new BigNumber(Math.floor(Math.random() * 50000 + 10000)),
      // myDeposit: new BigNumber(0),
      // peerDeposit: new BigNumber(0),
      timeout: new BigNumber(10)
    } as NodeTypes.ProposeInstallVirtualParams,
    requestId: generateUUID(),
    type: NodeTypes.MethodName.PROPOSE_INSTALL_VIRTUAL
  } as NodeTypes.MethodRequest;
}

export function makeTTTAppInstanceProposalReq(
  proposedByIdentifier: string,
  proposedToIdentifier: string,
  appId: Address
): NodeTypes.MethodRequest {
  return {
    params: {
      proposedToIdentifier,
      proposedByIdentifier,
      appId,
      initialState: {
        players: [
          xkeyKthAddress(proposedByIdentifier, 0),
          xkeyKthAddress(proposedToIdentifier, 0)
        ],
        turnNum: 0,
        winner: 0, // Hard-coded winner for test
        board: [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
      },
      abiEncodings: {
        stateEncoding:
          "tuple(address[2] players, uint256 turnNum, uint256 winner, uint256[3][3] board)",
        actionEncoding:
          "tuple(uint8 actionType, uint256 playX, uint256 playY, tuple(uint8 winClaimType, uint256 idx) winClaim)"
      },
      asset: {
        assetType: AssetType.ETH
      },
      myDeposit: new BigNumber(Math.floor(Math.random() * 50000 + 10000)),
      peerDeposit: new BigNumber(Math.floor(Math.random() * 50000 + 10000)),
      // myDeposit: new BigNumber(0),
      // peerDeposit: new BigNumber(0),
      timeout: new BigNumber(10)
    } as NodeTypes.ProposeInstallParams,
    requestId: generateUUID(),
    type: NodeTypes.MethodName.PROPOSE_INSTALL
  } as NodeTypes.MethodRequest;
}

export function makeGetAppInstanceInfoRequest(
  appId: string
): NodeTypes.MethodRequest {
  return {
    requestId: generateUUID(),
    type: NodeTypes.MethodName.GET_APP_INSTANCE_DETAILS,
    params: {
      appInstanceId: appId
    } as NodeTypes.GetAppInstanceDetailsParams
  };
}

export function makeGetAppInstanceRequest(
  appId: string
): NodeTypes.MethodRequest {
  return {
    requestId: generateUUID(),
    type: NodeTypes.MethodName.GET_APP_INSTANCE,
    params: {
      appInstanceId: appId
    } as NodeTypes.GetAppInstanceParams
  };
}

export function makeGetAppInstancesRequest(): NodeTypes.MethodRequest {
  return {
    requestId: generateUUID(),
    type: NodeTypes.MethodName.GET_APP_INSTANCES,
    params: {} as NodeTypes.GetAppInstancesParams
  };
}

export function makeGetChannelFromCounterpartyRequest(
  addr: string
): NodeTypes.MethodRequest {
  return {
    requestId: generateUUID(),
    type: NodeTypes.MethodName.GET_CHANNEL_ADDRESS_FROM_COUNTER,
    params: {
      counterpartyXpub: addr
    } as NodeTypes.GetChannelAddressFromCounterParams
  };
}

export function makeGetAllChannelsRequest(): NodeTypes.MethodRequest {
  return {
    requestId: generateUUID(),
    type: NodeTypes.MethodName.GET_CHANNEL_ADDRESSES,
    params: {} as NodeTypes.GetChannelAddressesParams
  };
}

export function makeGetFreeBalanceRequest(
  addr: string
): NodeTypes.MethodRequest {
  return {
    requestId: generateUUID(),
    type: NodeTypes.MethodName.GET_FREE_BALANCE_STATE,
    params: {
      multisigAddress: addr
    } as NodeTypes.GetFreeBalanceStateParams
  };
}

export function makeWithdrawRequest(
  multisigAddress: string,
  amount: BigNumber
): NodeTypes.MethodRequest {
  return {
    requestId: generateUUID(),
    type: NodeTypes.MethodName.WITHDRAW,
    params: {
      multisigAddress,
      amount
    } as NodeTypes.WithdrawParams
  };
}
