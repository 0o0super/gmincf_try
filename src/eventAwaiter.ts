import {
  Node,
  NODE_EVENTS,
  NodeEvents,
  ProposeVirtualMessage
} from "@counterfactual/node/dist";
// import { Node as NodeTypes } from "@counterfactual/types";

export default class EventAwaiter {
  private eventListener: Map<NodeEvents, ((ret: any) => void)[]> = new Map();
  private eventQueue: Map<NodeEvents, any[]> = new Map();
  constructor(node: Node) {
    Object.values(NODE_EVENTS).forEach(ev => {
      this.eventQueue.set(ev, []); // most event are unwanted!
      this.subscriptEvent(node, ev);
    });
  }

  private subscriptEvent(node: Node, event: NodeEvents) {
    node.on(event, async (data: any) => {
      // console.log(`Event ${event} firing by node ${node.publicIdentifier.slice(6, 15)}`);
      const eventResolve = this.eventListener.get(event);
      if (eventResolve) {
        // if (data.data) data = data.data;
        eventResolve.forEach(cb => setTimeout(() => cb(data), 0)); // make it run next tick
        this.eventListener.delete(event);
      } else {
        // if event is not handled by await, then queue it
        this.eventQueue.set(event, this.eventQueue.get(event)!.concat([data]));
      }
    });
  }

  handleEvent(event: NodeEvents): any[] {
    return this.eventQueue.get(event)!;
  }

  clearEvent(event: NodeEvents) {
    this.eventQueue.set(event, []);
  }

  awaitEvent(event: NodeEvents) {
    return new Promise(resolve => {
      if (this.eventListener.has(event)) {
        const callbacks = this.eventListener.get(event)!;
        this.eventListener.set(event, callbacks.concat([resolve]));
      } else {
        this.eventListener.set(event, [resolve]);
      }
    });
  }
}
