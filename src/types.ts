export type PeerId = any;
export type PeerInfo = any;
export type ConnManager = any;
export type P2PMessage = {
  action: string;
  toCFAddr: string[];
  payload: any;
};

export enum CFMessageTypes {
  INFO = "info",
  INTRODUTION = "intro",
  OFFER_PEERS = "offer_peers",
  INITIALIZED = "initialized",
  ALL_INI = "allInitialized",
  BASE_OK = "baseNetworkCompleted",
  ALL_BASE_OK = "allBaseNetworkCompleted",
  FINISHED = "finished",
  FORCE_STOP = "force_stop",
  ROUND_END = "round_end"
}

export type CFMessage = {
  id: string;
  type: CFMessageTypes;
  payload: any;
};

export type OfferPeerPayload = {
  p2pPeers: { id: string; addr: string[] }[];
  cfAddrs: string[];
  channelMap: { [addr: string]: string[] };
  minimumGraph: string[];
};

export type FinishReportPayload = {
  onChainTxCount: number;
  installStats: { length: number; time: number }[];
  uninstallStats: { length: number; time: number }[];
};
