import Axios from "axios";
import { exec as startProcess } from "child_process";
import { readFileSync } from "fs";
import Websocket from "ws";

import CounterfactualNode from "./CounterfactualNode";

async function getReadyWebsocket(path: string) {
  while (true) {
    try {
      const commanderWs = new Websocket(path);
      await new Promise((res, rej) => {
        commanderWs.on("open", res);
        commanderWs.on("error", rej);
      });
      commanderWs.setMaxListeners(Number.MAX_SAFE_INTEGER);
      return commanderWs;
    } catch (error) {
      console.error("Commander not ready, retrying...");
      await new Promise(r => setTimeout(r, 2000));
    }
  }
}

const main = async () => {
  if (process.argv[2] === "ganache") {
    startProcess(
      `node ${__dirname}/setup-env-ganache.js`,
      (err, stdout, stderr) => {
        if (err) console.error(err);
        if (stdout) console.log(stdout);
        if (stderr) console.error(stderr);
      }
    );
  } else if (process.argv[2] === "all-in-one") {
    let numNodes = 5;
    if (!isNaN(parseInt(process.argv[3], 10))) {
      numNodes = parseInt(process.argv[3], 10);
    }

    const commanderUrl = "http://127.0.0.1:3000";
    // start nodes
    for (let i = 1; i < numNodes; i += 1) {
      startProcess(
        `node ${__dirname}/CFNodeStarter.js --commander 127.0.0.1:3000`,
        (err, stdout, stderr) => {
          if (err) console.error(err);
          if (stdout) console.log(stdout);
          if (stderr) console.error(stderr);
        }
      );
    }

    // start main thread for debug
    while (true) {
      const commanderWs = await getReadyWebsocket(`${commanderUrl}/socket`);
      const roundConfig: any = (await Axios.get(
        `${commanderUrl}/currentRoundConfig`
      )).data;

      console.log(roundConfig);
      const node = new CounterfactualNode(
        roundConfig.nodes,
        roundConfig.provider,
        commanderUrl,
        commanderWs
      );
      try {
        await node.create();
      } catch (error) {
        console.error(error);
      }
      console.log("Node successfully stopped!");
      // clean up
      commanderWs.removeAllListeners();
      // grace time
      await new Promise(r => setTimeout(r, 1000));
    }
  } else {
    console.log("bye");
  }
};

main();
