import { AddressZero } from "ethers/constants";
import { JsonRpcProvider } from "ethers/providers";
import { fromMnemonic } from "ethers/utils/hdnode";
import { writeFileSync } from "fs";

import AutoNonceWallet from "./auto-nonce-wallet";
import configureNetworkContext from "./contractDeployment";

const ganache = require("ganache-core");

let provider: JsonRpcProvider;
let ganacheServer: {
  listen: (arg0: number, arg1: any) => void;
  close: () => void;
};

const GANACHE_PORT = 8545;
const MASTER_PK =
  "0x5a6d87e876a4a3775c93df2fb454f09ee6c120b19f0dbddac1553ff1cdca56e6";

const setupGanache = (mnemonics: string[]) => {
  const ganacheAccounts = mnemonics.map(mn => {
    return {
      balance: "120000000000000000",
      secretKey: fromMnemonic(mn).derivePath("m/44'/60'/0'/25446").privateKey
    };
  });
  ganacheServer = ganache.server({ accounts: ganacheAccounts });
  // @ts-ignore
  ganacheServer.listen(GANACHE_PORT, () => {});
};

const setupEnv = async (num: Number) => {
  // const mnemonics: string[] = [];
  provider = new JsonRpcProvider(`http://127.0.0.1:${GANACHE_PORT}`);

  // for (let i = 0; i < num; i += 1) {
  //   mnemonics.push(Wallet.createRandom().mnemonic);
  // }

  // setupGanache(mnemonics);

  const deployNetworkContext = await configureNetworkContext(
    new AutoNonceWallet(MASTER_PK, provider)
  );
  console.log("Finish deploy contracts");
  const networkContext = {
    AppRegistry: AddressZero,
    ETHBalanceRefundApp: deployNetworkContext.ETHBalanceRefundApp,
    ETHBucket: deployNetworkContext.ETHBucket,
    MultiSend: AddressZero,
    NonceRegistry: AddressZero,
    StateChannelTransaction: AddressZero,
    ETHVirtualAppAgreement: AddressZero,
    MinimumViableMultisig: deployNetworkContext.MinimumViableMultisig,
    ProxyFactory: deployNetworkContext.ProxyFactory,
    TicTacToe: deployNetworkContext.TicTacToe
  };

  const randomPortTitle = Math.floor(Math.random() * 499) + 100;
  const config = {
    networkContext,
    randomPortTitle,
    provider_addr: `http://127.0.0.1:${GANACHE_PORT}`,
    faucetPrivateKey: MASTER_PK,
    num_nodes: num
  };

  writeFileSync(`${__dirname}/config.json`, JSON.stringify(config));
};

const main = async () => {
  console.log("WHEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
  if (isNaN(parseInt(process.argv[2], 10))) {
    await setupEnv(5);
  } else {
    const nodeNum = parseInt(process.argv[2], 10);
    await setupEnv(nodeNum);
  }
};

main();
