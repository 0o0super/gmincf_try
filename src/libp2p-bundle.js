'use strict'
const TCP = require('libp2p-tcp')
// const MulticastDNS = require('libp2p-mdns')
const spdy = require('libp2p-spdy')
const KadDHT = require('libp2p-kad-dht')
const secio = require('libp2p-secio')
const defaultsDeep = require('@nodeutils/defaults-deep')
const libp2p = require('libp2p')

class Node extends libp2p {
  constructor (_options) {
    const defaults = {
      modules: {
        transport: [ TCP ],
        streamMuxer: [ spdy ],
        connEncryption: [ secio ],
        // peerDiscovery: [ MulticastDNS ],
        dht: KadDHT
      },
      config: {
        // peerDiscovery: {
        //   mdns: {
        //     interval: 2000,
        //     enabled: true
        //   }
        // },
        dht: {
          kBucketSize: 20
        },
        EXPERIMENTAL: {
          pubsub: true
        }
      }
    }
    super(defaultsDeep(_options, defaults))
  }
}

module.exports = Node;