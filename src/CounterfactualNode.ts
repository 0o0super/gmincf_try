import {
  CreateChannelMessage,
  DepositConfirmationMessage,
  // ERRORS,
  InstallMessage,
  InstallVirtualMessage,
  Node,
  NODE_EVENTS,
  // UninstallVirtualMessage,
  ProposeMessage,
  ProposeVirtualMessage,
  UninstallMessage,
  UninstallVirtualMessage,
  UpdateStateMessage
} from "@counterfactual/node/dist";
import { AppInstance } from "@counterfactual/node/dist/models";
import { Node as NodeTypes } from "@counterfactual/types";
import axios from "axios";
import { JsonRpcProvider } from "ethers/providers";
import { BigNumber, computeAddress, id } from "ethers/utils";
import { fromExtendedKey, fromMnemonic, HDNode } from "ethers/utils/hdnode";
import Websocket from "ws";

import AutoNonceWallet from "./auto-nonce-wallet";
import ChannelGraph from "./ChannelGraph";
import LokiStoreService from "./loki-store";
import P2PMessageService from "./P2PMsgService";
import { CFMessage, CFMessageTypes, FinishReportPayload } from "./types";
import {
  getMultisigCreationTransactionHash,
  makeDepositRequest,
  makeGetAllChannelsRequest,
  makeGetAppInstanceInfoRequest,
  makeGetAppInstanceRequest,
  makeGetFreeBalanceRequest,
  makeInstallRequest,
  makeInstallVirtualRequest,
  makeTTTAppInstanceProposalReq,
  makeTTTVirtualAppInstanceProposalReq,
  makeUninstallRequest,
  makeUninstallVirtualRequest,
  makeWithdrawRequest,
  updateAppRequest
} from "./utils";

function xkeyKthAddress(xkey: string, k: number): string {
  return computeAddress(xkeyKthHDNode(xkey, k).publicKey);
}

function xkeyKthHDNode(xkey: string, k: number): HDNode {
  return fromExtendedKey(xkey).derivePath(`${k}`);
}

function shaSortXpub(xpubs: string[]): string[] {
  const sorted = xpubs.sort();
  const sortedJoin = sorted.map(a => a.replace("xpub", "")).join("");
  const hashed = id(sortedJoin);
  const lastDigit = new BigNumber(`0x${hashed.slice(-1)}`).toNumber();
  if (lastDigit % 2 === 0) {
    return sorted;
  }
  return sorted.reverse();
}

export default class CounterfactualNode {
  private node!: Node;
  private messagingService = new P2PMessageService();
  private minimumGraph: string[] = [];
  private isInMinimumPahse = true;
  private minimumOkSwitch!: () => void;
  private completeGraph!: ChannelGraph;
  private knownPeer: Set<string> = new Set();
  private actedPeer: Set<string> = new Set();
  private finishedPeer: Set<string> = new Set();
  private donePeer: Set<string> = new Set();
  private finishSwitch!: () => void;
  private networkContext: any;

  private recorder: FinishReportPayload = {
    onChainTxCount: 0,
    installStats: [],
    uninstallStats: []
  };

  constructor(
    private numNodes: number,
    private providerAddr: string,
    private commanderAddr: string,
    private commanderWs: Websocket
  ) {
    console.log(this.providerAddr);
    // this.create();
  }

  public async create() {
    const storeService = new LokiStoreService();
    const nodeConfig = {
      STORE_KEY_PREFIX: "GminWheee"
    };
    const provider = new JsonRpcProvider(this.providerAddr);
    const mnemonic = AutoNonceWallet.createRandom().mnemonic;
    const myAddress = fromMnemonic(mnemonic).derivePath("m/44'/60'/0'/25446")
      .address;

    const getEtherResp = await axios.get(
      `${this.commanderAddr}/faucet/${myAddress}`
    );
    const { data: networkContext } = await axios.get(
      `${this.commanderAddr}/networkContext`
    );
    this.networkContext = networkContext;

    const txHash: string = getEtherResp.data.tx;
    await provider.waitForTransaction(txHash, 1);

    storeService.set([{ key: "MNEMONIC", value: mnemonic }]);
    this.node = await Node.create(
      this.messagingService,
      storeService,
      nodeConfig,
      provider,
      networkContext,
      2
    );

    console.error = (msg: any) => {
      const stringMsg = JSON.stringify(msg);
      this.commanderWs.send(
        JSON.stringify({
          id: this.node.publicIdentifier,
          type: CFMessageTypes.INFO,
          payload: `${this.node.publicIdentifier.slice(
            6,
            12
          )} [WARN] : ${stringMsg}`
        } as CFMessage)
      );
    };

    const payloadFromCommander = await this.messagingService.initialize(
      this.node.publicIdentifier,
      10000 + Math.floor(Math.random() * 29999),
      this.commanderWs
    );
    payloadFromCommander.cfAddrs.forEach(p => this.knownPeer.add(p));
    this.minimumGraph = payloadFromCommander.minimumGraph;
    this.completeGraph = new ChannelGraph(this.node.publicIdentifier);
    this.completeGraph.importGraph(payloadFromCommander.channelMap);

    // should be here?
    this.commanderWs.send(
      JSON.stringify({
        id: this.node.publicIdentifier,
        type: CFMessageTypes.INITIALIZED,
        payload: this.node.publicIdentifier
      } as CFMessage)
    );
    // wait everyone initialized!
    await new Promise(r =>
      this.commanderWs.on("message", (data: string) => {
        const msg: CFMessage = JSON.parse(data);
        if (msg.type === CFMessageTypes.ALL_INI) {
          r();
        }
      })
    );

    // registe event
    this.subscribEvents();

    // console.error("UP!!!");
    console.log("P2P messaging initialized");

    this.constructMinimumNetwork();
    await new Promise(r => (this.minimumOkSwitch = r));
    this.commanderWs.send(
      JSON.stringify({
        id: this.node.publicIdentifier,
        type: CFMessageTypes.BASE_OK,
        payload: this.recorder
      } as CFMessage)
    );
    await new Promise(r =>
      this.commanderWs.on("message", (data: string) => {
        const msg: CFMessage = JSON.parse(data);
        if (msg.type === CFMessageTypes.ALL_BASE_OK) {
          r();
        }
      })
    );

    // incase this round complete = minimum
    this.transactWithOthers();
    this.init();
    await new Promise(r => (this.finishSwitch = r));

    // ???should these be here???
    // tell commander I'm done!
    this.commanderWs.send(
      JSON.stringify({
        id: this.node.publicIdentifier,
        type: CFMessageTypes.FINISHED,
        payload: this.recorder
      } as CFMessage)
    );

    // wait everyone done!
    await new Promise(r =>
      this.commanderWs.on("message", (data: string) => {
        const msg: CFMessage = JSON.parse(data);
        if (msg.type === CFMessageTypes.ROUND_END) r();
      })
    );
    // stop p2p node
    await this.messagingService.stop();
    // console.log("Everyone done!!!!!!!!");
  }

  private constructMinimumNetwork() {
    const myIndex = this.minimumGraph.indexOf(this.node.publicIdentifier);
    const myMinimumNeighbor = [];
    if (myIndex !== 0) {
      myMinimumNeighbor.push(this.minimumGraph[myIndex - 1]);
    }
    if (myIndex !== this.minimumGraph.length - 1) {
      myMinimumNeighbor.push(this.minimumGraph[myIndex + 1]);
    }

    for (const n of myMinimumNeighbor) {
      if (
        shaSortXpub([n, this.node.publicIdentifier])[0] ===
        this.node.publicIdentifier
      ) {
        // me construct channel!
        this.createChannel(n);
      }
    }
  }

  private init() {
    const currentGraph = this.messagingService.getChannelGraph();
    const neighborsToAct = this.completeGraph
      .getMyConnections()
      .filter(e => !currentGraph.getMyConnections().includes(e));

    for (const n of neighborsToAct) {
      if (
        shaSortXpub([n, this.node.publicIdentifier])[0] ===
        this.node.publicIdentifier
      ) {
        // create channel!!
        // create channel send 1 tx on-chain
        this.recorder.onChainTxCount += 1;
        this.createChannel(n);
      }
    }
  }

  private transactWithOthers() {
    const graph = this.messagingService.getChannelGraph();
    const peersExceptMe = new Set(this.knownPeer);
    peersExceptMe.delete(this.node.publicIdentifier);

    for (const p of peersExceptMe) {
      if (this.actedPeer.has(p)) continue;
      const currentPath = graph.getIntermediariesBetweenMeAnd(p);
      const completePath = this.completeGraph.getIntermediariesBetweenMeAnd(p);
      // there always a path in complete graph, so not [] condition
      if (currentPath.join() === completePath.join()) {
        // this path is final!!
        this.actedPeer.add(p);
        if (currentPath.length === 2) {
          // go go ledger channel!
          this.proposeOrProposeVirtual(p);
        } else {
          // go go virtual channel!
          const inters = currentPath.filter(
            a => a !== p && a !== this.node.publicIdentifier
          );
          this.proposeOrProposeVirtual(p, inters);
        }
      } else {
        // for debug!!
        // if (currentPath.length === completePath.length) {
        //   console.error(
        //     `Current: ${currentPath
        //       .map(a => a.slice(6, 12))
        //       .join()}, Complete: ${completePath
        //       .map(a => a.slice(6, 12))
        //       .join()}`
        //   );
        // }
      }
    }
  }

  private subscribEvents() {
    this.messagingService.on("graphEdgeAdded", async () => {
      // minimum constructed if path is the same!
      if (this.isInMinimumPahse) {
        const graph = this.messagingService.getChannelGraph();
        const path = graph.breadFirstSearch(
          this.minimumGraph[0],
          this.minimumGraph[this.minimumGraph.length - 1]
        );
        if (path.join() === this.minimumGraph.join()) {
          // minimum ok!
          this.isInMinimumPahse = false;
          this.minimumOkSwitch();
        }
      } else {
        this.transactWithOthers();
      }
    });

    this.messagingService.on("allOperationDone", async (addr: string) => {
      this.donePeer.add(addr);
      if (this.donePeer.size === this.numNodes - 1) {
        // we can withdraw now!!
        this.donePeer.add("done");
        // console.error("done");
        await this.withdraw();
      }
    });

    this.commanderWs.on("message", (data: string) => {
      const msg: CFMessage = JSON.parse(data);
      if (msg.type === CFMessageTypes.FORCE_STOP) {
        console.log(`FORCE STOPPED!!!!! REASON: ${msg.payload.reason}`);
        this.finishSwitch();
      }
    });

    // When channel created
    this.node.on(
      NODE_EVENTS.CREATE_CHANNEL,
      async (data: CreateChannelMessage) => {
        // deposit some funds
        const result = data.data;
        const iniShort = result.initiatingXpub.substr(5, 10);
        const resShort = result.respondingXpub.substr(5, 10);
        console.log(
          `Channel ${iniShort} => ${resShort} @ ${result.multisigAddress}`
        );

        const sortedXpub = shaSortXpub([
          data.data.respondingXpub,
          data.data.initiatingXpub
        ]);

        if (this.node.publicIdentifier === data.data.respondingXpub) {
          // deposit also do one on-chain tx
          this.recorder.onChainTxCount += 1;
          await this.depositOnChannel(result.multisigAddress!);
        } else {
          // wait smaller one to initiate deposit
          await new Promise(async res => {
            this.node.on(
              NODE_EVENTS.DEPOSIT_CONFIRMED,
              (msg: DepositConfirmationMessage) => {
                if (msg.data.multisigAddress === data.data.multisigAddress!) {
                  // make sure it's same channel
                  res();
                }
              }
            );
          });
        }

        if (this.node.publicIdentifier === data.data.initiatingXpub) {
          // deposit also do one on-chain tx
          this.recorder.onChainTxCount += 1;
          await this.depositOnChannel(result.multisigAddress!);
        } else {
          // wait bigger one to initiate deposit
          await new Promise(async res => {
            this.node.on(
              NODE_EVENTS.DEPOSIT_CONFIRMED,
              (msg: DepositConfirmationMessage) => {
                if (msg.data.multisigAddress === data.data.multisigAddress!) {
                  // make sure it's same channel
                  res();
                }
              }
            );
          });
          // this.messagingService.channelDepositComplete(sortedXpub);
        }
        this.messagingService.channelDepositComplete(sortedXpub);
      }
    );

    // When someone proposed install
    this.node.on(NODE_EVENTS.PROPOSE_INSTALL, async (data: ProposeMessage) => {
      // console.log(`Got proposed app ${data.data.appInstanceId!}`);
      const req = makeInstallRequest(data.data.appInstanceId!);
      // let counter = 0;
      // while (counter < 8) {
      const current = Date.now();
      await this.node.call(req);
      this.recorder.installStats.push({
        length: 1,
        time: Date.now() - current
      });
      // }
      // console.log("Install app done!!!!!!!!!!!");
    });

    // When someone proposed virtual
    this.node.on(
      NODE_EVENTS.PROPOSE_INSTALL_VIRTUAL,
      async (data: ProposeVirtualMessage) => {
        // only install virtual if I am recepient!!!!
        // console.log(`Got proposed virtual ${data.data.appInstanceId!}`);
        if (data.data.proposedToIdentifier === this.node.publicIdentifier) {
          const installVirtualReq = makeInstallVirtualRequest(
            data.data.appInstanceId!,
            data.data.intermediaries
          );
          // console.log(
          //   `Starting installVirtual request with id ${
          //     installVirtualReq.requestId
          //   }`
          // );
          const current = Date.now();
          await this.node.call(installVirtualReq);
          this.recorder.installStats.push({
            length: data.data.intermediaries.length + 1,
            time: Date.now() - current
          });
          console.log(
            `Install virtual len = ${data.data.intermediaries.length +
              1} time = ${Date.now() - current}ms`
          );
          // console.log("Install virtual done!!!!!!!!!!!");
        }
      }
    );

    this.node.on(NODE_EVENTS.INSTALL, async (data: InstallMessage) => {
      // send update to counter party
      const appId = data.data.installedId!;
      if (!appId) {
        throw Error("NOT INSATLLED");
      }
      const getAppInstancesRequest = makeGetAppInstanceInfoRequest(appId);
      const response: NodeTypes.MethodResponse = await this.node.callStatic(
        getAppInstancesRequest
      );
      const res = response.result as NodeTypes.GetAppInstanceDetailsResult;
      const sortedXpub = shaSortXpub([
        res.appInstance.proposedByIdentifier,
        res.appInstance.proposedToIdentifier
      ]);
      if (sortedXpub[0] === this.node.publicIdentifier) {
        // await sleep(2000);
        this.updateApp(appId, sortedXpub[1], 1);
      }
    });

    this.node.on(
      NODE_EVENTS.INSTALL_VIRTUAL,
      async (data: InstallVirtualMessage) => {
        // send update to counter party
        const appId = data.data.installedId!;
        if (!appId) {
          throw Error("NOT INSATLLED VIRTUAL");
        }
        const getAppInstancesRequest = makeGetAppInstanceInfoRequest(appId);
        const response: NodeTypes.MethodResponse = await this.node.callStatic(
          getAppInstancesRequest
        );
        const res = response.result as NodeTypes.GetAppInstanceDetailsResult;
        const sortedXpub = shaSortXpub([
          res.appInstance.proposedByIdentifier,
          res.appInstance.proposedToIdentifier
        ]);
        if (sortedXpub[0] === this.node.publicIdentifier) {
          this.updateApp(appId, sortedXpub[0], 1);
        }
      }
    );

    this.node.on(NODE_EVENTS.UPDATE_STATE, async (data: UpdateStateMessage) => {
      const appId = data.data.appInstanceId;
      const getAppInstanceRequest = makeGetAppInstanceRequest(appId);
      const appInstanceRes: NodeTypes.MethodResponse = await this.node.callStatic(
        getAppInstanceRequest
      );
      const app: AppInstance = (appInstanceRes.result as NodeTypes.GetAppInstanceResult)
        .appInstance;

      const getAppInstanceInfoRequest = makeGetAppInstanceInfoRequest(appId);
      const appInfo = (await this.node.callStatic(getAppInstanceInfoRequest))
        .result as NodeTypes.GetAppInstanceDetailsResult;

      const sortedXpub = shaSortXpub([
        appInfo.appInstance.proposedByIdentifier,
        appInfo.appInstance.proposedToIdentifier
      ]);
      if (sortedXpub[app.nonce % 2] === this.node.publicIdentifier) {
        if (app.nonce > 2) {
          // construct uninstall
          let req: NodeTypes.MethodRequest;
          if (
            appInfo.appInstance.intermediaries &&
            appInfo.appInstance.intermediaries.length > 0
          ) {
            req = makeUninstallVirtualRequest(appId);
          } else {
            req = makeUninstallRequest(appId);
          }

          console.log(
            `Starting uninstall ${appId} request with id ${req.requestId}`
          );
          const timer = Date.now();
          await this.node.call(req);
          this.recorder.uninstallStats.push({
            length: (appInfo.appInstance.intermediaries || []).length + 1,
            time: Date.now() - timer
          });
          console.log(
            `Uninstall len = ${(appInfo.appInstance.intermediaries || [])
              .length + 1} time = ${Date.now() - timer}ms`
          );
        } else {
          // I'll initiate state update!
          // console.log(`Updating app ${appId} for nonce ${app.nonce}`);
          this.updateApp(
            appId,
            sortedXpub.filter(a => a !== this.node.publicIdentifier)[0],
            app.nonce
          );
        }
      }
    });

    this.node.on(NODE_EVENTS.UNINSTALL, (msg: UninstallMessage) =>
      this.uninstalledEventHandler(msg)
    );
    this.node.on(
      NODE_EVENTS.UNINSTALL_VIRTUAL,
      (msg: UninstallVirtualMessage) => this.uninstalledEventHandler(msg)
    );
  }

  private async createChannel(counterPartyXpub: string) {
    await getMultisigCreationTransactionHash(this.node, [
      this.node.publicIdentifier,
      counterPartyXpub
    ]);
  }

  private async depositOnChannel(multisigAddress: string) {
    // console.log(`Deposit start on ${multisigAddress}`);
    const depositReq = makeDepositRequest(
      multisigAddress,
      new BigNumber("1000000000000")
    );
    // console.log(`Starting deposit request with id ${depositReq.requestId}`);
    await this.node.call(depositReq);
    // console.log("Channel deposit done");
  }

  private async proposeOrProposeVirtual(to: string, intermediaries?: string[]) {
    const sortedId = shaSortXpub([to, this.node.publicIdentifier]);
    if (this.node.publicIdentifier === sortedId[0]) {
      // I'll propose!
      // const getAddrReq = makeGetChannelFromCounterpartyRequest(peer);
      // const { multisigAddress } = (await this.node.callStatic(getAddrReq))
      //   .result as NodeTypes.GetChannelAddressFromCounterResult;

      let req: NodeTypes.MethodRequest;
      if (!intermediaries) {
        // we got ledger channel with this peer!
        req = makeTTTAppInstanceProposalReq(
          this.node.publicIdentifier,
          to,
          this.networkContext.TicTacToe
        );
        // console.error(
        //   `Making propose in virtual channel with path ${this.node.publicIdentifier.slice(
        //     6,
        //     12
        //   )} => ${to.slice(6, 12)}!`
        // );
      } else {
        // we need install virtual with this peer!
        req = makeTTTVirtualAppInstanceProposalReq(
          this.node.publicIdentifier,
          to,
          this.networkContext.TicTacToe,
          intermediaries
        );
        const path = [this.node.publicIdentifier, ...intermediaries, to]
          .map(a => a.slice(6, 12))
          .join(" => ");
        console.log(`Making propose in virtual channel with path ${path}!`);
      }

      this.node.call(req);
    }
  }

  async updateApp(virtualAppId: string, counterParty: string, turnNum: number) {
    const updateReq = updateAppRequest(virtualAppId, {
      turnNum,
      players: [
        xkeyKthAddress(this.node.publicIdentifier, 0),
        xkeyKthAddress(counterParty, 0)
      ],
      winner: 3,
      board: [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    });
    await this.node.call(updateReq);
  }

  async uninstalledEventHandler(data: UninstallVirtualMessage) {
    const getAppInstanceInfoRequest = makeGetAppInstanceInfoRequest(
      data.data.appInstanceId
    );
    const appInfo = (await this.node.callStatic(getAppInstanceInfoRequest))
      .result as NodeTypes.GetAppInstanceDetailsResult;
    const appParticipants = [
      appInfo.appInstance.proposedByIdentifier,
      appInfo.appInstance.proposedToIdentifier
    ];
    if (appInfo.appInstance.intermediaries) {
      console.log("Virtual app uninstalled!!");
    } else {
      console.log("App uninstalled!!");
    }
    if (appParticipants.includes(this.node.publicIdentifier)) {
      const cp = appParticipants.filter(
        a => a !== this.node.publicIdentifier
      )[0];
      this.finishedPeer.add(cp);

      // console.error(`Finished ${this.finishedPeer.size}`);
      // console.error(`Acted ${this.actedPeer.size}`);
      if (this.finishedPeer.size === this.numNodes - 1) {
        this.messagingService.allOperationDone();
        this.finishedPeer.add("done");
      }
    }
  }

  async withdraw() {
    const req = makeGetAllChannelsRequest();
    const allMultiSig = (await this.node.callStatic(req))
      .result as NodeTypes.GetChannelAddressesResult;
    const withdrawPromises: Promise<void>[] = [];
    for (const addr of allMultiSig.multisigAddresses) {
      try {
        const getFBStateReq = makeGetFreeBalanceRequest(addr);
        const { state } = (await this.node.callStatic(getFBStateReq))
          .result as NodeTypes.GetFreeBalanceStateResult;
        const address = xkeyKthAddress(this.node.publicIdentifier, 0);
        let amount: BigNumber;
        if (address === state.alice) {
          amount = state.aliceBalance;
        } else {
          amount = state.bobBalance;
        }

        // initiate withdraw
        const withdrawReq = makeWithdrawRequest(addr, amount);
        // withdraw also do one on-chain tx!
        this.recorder.onChainTxCount += 1;
        console.log(`Withdraw ${amount.toString()} from ${addr} starting!`);
        // don't wait!, will boom??
        const withdrawPromise = this.node
          .call(withdrawReq)
          .then(() =>
            console.log(`Withdraw ${amount.toString()} from ${addr} finished!`)
          );
        withdrawPromises.push(withdrawPromise);
      } catch (error) {
        // virtual channels doesn't have FB, so we ignore it
      }
    }
    // wait all withdraw done!
    await Promise.all(withdrawPromises);
    // ALL FINISHED!!
    this.finishSwitch();
  }
}
