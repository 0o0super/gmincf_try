import { IStoreService } from "@counterfactual/node/dist";
import Loki from "lokijs";

type Data = {
  key: string;
  value: any;
};

export default class LokiStoreService implements IStoreService {
  private store: Loki;

  constructor() {
    this.store = new Loki("store");
  }

  private getCollectionNameAndItem(key: string): [string, string] {
    if (!key.includes("/")) return [key, key];

    const [prefix, xpub, collection, item] = key.split("/");
    const collectionName = [prefix, xpub, collection].join("/");
    return [collectionName, item];
  }

  async get(key: string): Promise<any> {
    const [collectionName, item] = this.getCollectionNameAndItem(key);
    const coll: Collection<Data> = this.store.getCollection(collectionName);
    if (!coll) {
      return null;
    }

    let result: any;
    if (!item) {
      result = (coll.data as Data[]).reduce((p, c) => {
        // @ts-ignore
        p[c.key] = c.value;
        return p;
      }, {});
    } else {
      result = coll.findOne({ key: item });
      if (result) {
        result = result.value;
      }
    }

    return result;
  }

  async set(pairs: { key: string; value: any }[]): Promise<boolean> {
    for (const pair of pairs) {
      const [collectionName, item] = this.getCollectionNameAndItem(pair.key);
      let coll: Collection<Data> = this.store.getCollection(collectionName);
      if (!coll) {
        coll = this.store.addCollection(collectionName);
      }

      const data: Data | null = coll.findOne({ key: item });
      if (data) {
        data.value = pair.value;
        coll.update(data);
      } else {
        coll.insert({ key: item, value: pair.value });
      }
    }
    // await new Promise(r => this.store.save(r));
    return true;
  }
}
