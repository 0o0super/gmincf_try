import { IStoreService } from "@counterfactual/node/dist";

export default class MemAndShowStoreService implements IStoreService {
  private store: Map<string, any> = new Map();
  constructor() {}
  async get(key: string): Promise<any> {
    console.log(`Getting ${key}`);
    if (this.store.get(key)) {
      const gotVal = this.store.get(key);
      // console.log(`${gotVal}/${gotVal.leaves}`);
      if (gotVal.leaves) {
        const retVal = {};
        gotVal.leaves.forEach((e: string) => {
          // @ts-ignore
          retVal[e] = this.store.get(`${key}/${e}`);
        });
        return retVal;
      }
      return this.store.get(key);
    }
    // console.log(`Getting non existing key: ${key}`);
    return Promise.resolve(null);
  }

  async set(pairs: { key: string; value: any }[]): Promise<boolean> {
    for (const pair of pairs) {
      // THIS WILL FAIL?
      const splited = pair.key.split("/");
      for (let i = 0; i < splited.length - 1; i += 1) {
        const entry = splited.slice(0, i + 1).join("/");
        if (this.store.has(entry)) {
          const data = this.store.get(entry).leaves as string[];
          if (!data.includes(splited[i + 1])) {
            this.store.set(entry, { leaves: data.concat(splited[i + 1]) });
          }
        } else {
          this.store.set(entry, { leaves: [splited[i + 1]] });
        }
      }
      // if(pair.key.includes("channel")) console.log(`Setting ${pair.key}/${pair.value}`)
      // actually set value
      this.store.set(pair.key, pair.value);
    }
    return true;
  }
}
