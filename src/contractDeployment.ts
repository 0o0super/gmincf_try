import { Contract, ContractFactory } from "ethers";

import AutoNonceWallet from "./auto-nonce-wallet.js";
import BalanceRefundApp from "./contracts/ETHBalanceRefundApp.json";
import ETHBucket from "./contracts/ETHBucket.json";
import MinimumViableMultisig from "./contracts/MinimumViableMultisig.json";
import ProxyFactory from "./contracts/ProxyFactory.json";
import TicTacToeApp from "./contracts/TicTacToeApp.json";

export default async function configureNetworkContext(wallet: AutoNonceWallet) {
  const promiseToWait: Promise<Contract>[] = [];

  const balanceRefundContract = await new ContractFactory(
    BalanceRefundApp.abi,
    BalanceRefundApp.bytecode,
    wallet
  ).deploy();
  promiseToWait.push(balanceRefundContract.deployed());

  const mvmContract = await new ContractFactory(
    MinimumViableMultisig.abi,
    MinimumViableMultisig.bytecode,
    wallet
  ).deploy();
  promiseToWait.push(mvmContract.deployed());

  const proxyFactoryContract = await new ContractFactory(
    ProxyFactory.abi,
    ProxyFactory.bytecode,
    wallet
  ).deploy();
  promiseToWait.push(proxyFactoryContract.deployed());

  const ethBucketContract = await new ContractFactory(
    ETHBucket.abi,
    ETHBucket.bytecode,
    wallet
  ).deploy();
  promiseToWait.push(ethBucketContract.deployed());

  const tttContract = await new ContractFactory(
    TicTacToeApp.interface,
    TicTacToeApp.bytecode,
    wallet
  ).deploy();
  promiseToWait.push(tttContract.deployed());

  await Promise.all(promiseToWait);

  return {
    ETHBalanceRefundApp: balanceRefundContract.address,
    MinimumViableMultisig: mvmContract.address,
    ProxyFactory: proxyFactoryContract.address,
    ETHBucket: ethBucketContract.address,
    TicTacToe: tttContract.address
  };
}
