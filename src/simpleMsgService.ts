import { IMessagingService, NodeMessage } from "@counterfactual/node/dist";

class SimpleMessageService implements IMessagingService {
  private callbackStore: Map<string, (msg: NodeMessage) => void> = new Map();
  private networkDelay: number;
  constructor() {
    this.networkDelay = 0;
  }

  changeNetworkDelay(netowrkDelay: number): void {
    this.networkDelay = netowrkDelay;
  }

  async send(to: string[], msg: NodeMessage): Promise<void> {
    // console.log(`${msg.from.substr(6, 12)}  =>  ${to.substr(6, 12)}: ${msg.type}`);
    for (const t of to) {
      if (this.callbackStore.has(t)) {
        await new Promise(r => setTimeout(r, this.networkDelay));
        this.callbackStore.get(t)!(msg);
      } else {
        console.log(
          `ERR: sent to nobody :( ${msg.from.substr(6, 12)}  =>  ${t.substr(
            6,
            12
          )}: ${msg.type}`
        );
      }
    }
    return;
  }
  onReceive(address: string, callback: (msg: NodeMessage) => void): any {
    this.callbackStore.set(address, callback);
  }
}

const simpleMessageService = new SimpleMessageService();
export default simpleMessageService;
